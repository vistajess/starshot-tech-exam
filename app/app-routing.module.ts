import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ShellComponent } from './components/shell/shell.component';
import { PageOneComponent } from './components/page-one/page-one.component';
import { PageTwoComponent } from './components/page-two/page-two.component';
import { NotFoundComponent } from './components/not-found/not-found.component';


const routes: Routes = [
    {
        path: 'home',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: '', redirectTo: '/home/page-one', pathMatch: 'full'
    },
    {
        path: 'home',
        component: ShellComponent,
        children: [
            {
                path: 'page-one',
                component: PageOneComponent

            },
            {
                path: 'page-two',
                component: PageTwoComponent
            },
            // {
            //     path: 'parking-rentals',
            //     component: ParkingRentalsComponent
            // },
            // {
            //     path: 'garage/:id',
            //     component: GarageViewComponent,
            //     resolve: {
            //         garage: GarageByIdResolver,
            //         floorList: FloorListByGarageId,
            //     },
            // }
        ]
    },
    {
        path: '404', component: NotFoundComponent
    },
    {
        path: '**',
        redirectTo: '/404',
    }

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }