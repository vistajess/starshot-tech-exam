import { Headers, RequestOptions } from '@angular/http';

export class AppConfig {

  headers = new Headers({ 
    'Accept': `application/json`,
    'Content-Type': 'application/json'
  });

  options = new RequestOptions({ headers: this.headers });
}
