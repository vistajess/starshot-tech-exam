import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { AppConfig } from '../config/api.config';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {

  appConfig = new AppConfig();
  env = environment;

  constructor(
    private http: Http
  ) { }

  /**
  * get data from server
  *  
  */
  getData(): Observable<any[]> {
    return this.http.get(`${this.env.API_URL}data`, this.appConfig.options)
      .map((response: Response) => response.json());
  }

}
