import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[NumberOnly]'
})
export class NumberOnlyDirective {

  @Input() NumberOnly: boolean;

  constructor(
    private elementRef: ElementRef
  ) { }

  @HostListener('keydown', ['$event']) onKeyDown(event) {
    console.log(this.NumberOnly)
    let e = <KeyboardEvent> event;
    if (this.NumberOnly) {
      if ([46, 8, 9, 27, 13, 110, 190].indexOf(e.keyCode) !== -1 || (e.keyCode >= 35 && e.keyCode <= 39)) {
        return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
      }
    }
  }

}
