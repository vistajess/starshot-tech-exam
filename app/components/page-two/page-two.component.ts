import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

declare var echarts: any;

@Component({
  selector: 'app-page-two',
  templateUrl: './page-two.component.html',
  styleUrls: ['./page-two.component.css']
})
export class PageTwoComponent implements OnInit {

  private data: any[];
  private timer: number;
  private intervalFunc: any;

  constructor(
    private dataService: DataService
  ) { 
    this.timer = 10;
  }

  ngOnInit() {
    this.chartReinitialize();
  }

  chartReinitialize() {
    // Clear interval first before declaring another instance of `setInterval`
    clearInterval(this.intervalFunc);
    let intervalTimer = this.timer ? this.timer : 10;
    this.intervalFunc = setInterval(() => {
      this.getData();
    }, intervalTimer * 1000);
  }

  getData() {
    this.dataService.getData().subscribe((response: any) => {
      let dataArray = response.results.map(item => ({
        name: item.name,
        value: item.employees
      }));
      this.buildChart(dataArray);
    })
  }

  /**
   * Create the options for the chart to be initialized
   * 
   * @param dataArray array
   */
  buildChart(dataArray) {
    echarts.init(document.getElementById('main')).setOption({
      title: {
        text: 'Chart of Employees',
        subtext: '',
        x: 'center'
      },
      legend: {
        orient: 'vertical',
          left: 'left',
          data: dataArray.map(item => item.name)
      },
      tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
      },
      series: {
        type: 'pie',
        data: dataArray
      }
    });
  }

}
