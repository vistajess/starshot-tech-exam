import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ellipsis',
  templateUrl: './ellipsis.component.html',
  styleUrls: ['./ellipsis.component.scss']
})
export class EllipsisComponent implements OnInit {

  @Input('text') text: any;
  constructor() { }

  ngOnInit() {
  }

}
