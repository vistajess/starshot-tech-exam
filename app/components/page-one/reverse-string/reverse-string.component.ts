import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-reverse-string',
  templateUrl: './reverse-string.component.html',
  styleUrls: ['./reverse-string.component.scss']
})
export class ReverseStringComponent implements OnInit {

  @Input('text') text: any;
  constructor() { }

  ngOnInit() {

  }

}
