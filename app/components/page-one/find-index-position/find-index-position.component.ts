import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-find-index-position',
  templateUrl: './find-index-position.component.html',
  styleUrls: ['./find-index-position.component.scss']
})
export class FindIndexPositionComponent implements OnInit {

  @Input('text') text: any;
  constructor() { }

  ngOnInit() {
  }

}
