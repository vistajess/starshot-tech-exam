import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindIndexPositionComponent } from './find-index-position.component';

describe('FindIndexPositionComponent', () => {
  let component: FindIndexPositionComponent;
  let fixture: ComponentFixture<FindIndexPositionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindIndexPositionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindIndexPositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
