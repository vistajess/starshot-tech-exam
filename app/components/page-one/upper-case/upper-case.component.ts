import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-upper-case',
  templateUrl: './upper-case.component.html',
  styleUrls: ['./upper-case.component.scss']
})
export class UpperCaseComponent implements OnInit {

  @Input('text') text: any;
  constructor() { }

  ngOnInit() {
  }

}
