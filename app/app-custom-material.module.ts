import { NgModule } from '@angular/core';
import {
    MatCardModule,
    MatInputModule, 
    MatMenuModule, 
    MatSidenavModule,
    MatListModule
} from '@angular/material';

@NgModule({ 
    imports: [
      MatCardModule,
      MatInputModule,
      MatMenuModule,
      MatListModule
    ],
    exports: [
      MatInputModule,
      MatMenuModule,
      MatSidenavModule,
      MatListModule
    ]
})

export class CustomMaterialModule { }