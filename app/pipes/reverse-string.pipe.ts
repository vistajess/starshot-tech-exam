import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reverseString'
})
export class ReverseStringPipe implements PipeTransform {

  transform(str: any): any {
    let split = str.split("");
    let stringArray = split.reverse();

    return stringArray.join("");
  }

}
