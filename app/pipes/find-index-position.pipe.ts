import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'findIndexPosition'
})
export class FindIndexPositionPipe implements PipeTransform {

  transform(string: any): any {
    let sentence = "The quick brown fox jumps over the lazy dog";
    if (string === "") {
      return;
    }
    
    let pos = 0;
    while (true) {
      let foundPos = sentence.indexOf(string, pos);
      if (foundPos == -1) break;
      pos = foundPos + 1;
    }
    return pos;
  }

}
