import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ellipsisWord'
})
export class EllipsisPipe implements PipeTransform {

  transform(value: string, limit: string, trail: string): any {
    console.log(value)
    let limitChar = limit != null ? parseInt(limit) : 10;
    let trailString = (trail != null) ? trail : '...';
    return value.length > limitChar ? value.substring(0, limitChar) + trailString : value;
  }

}
