import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import * as echarts from 'echarts';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { ShellComponent } from './components/shell/shell.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { PageOneComponent } from './components/page-one/page-one.component';
import { PageTwoComponent } from './components/page-two/page-two.component';
import { CustomMaterialModule } from './app-custom-material.module';
import { ReverseStringPipe } from './pipes/reverse-string.pipe';
import { ReverseStringComponent } from './components/page-one/reverse-string/reverse-string.component';
import { UpperCasePipe } from './pipes/upper-case.pipe';
import { UpperCaseComponent } from './components/page-one/upper-case/upper-case.component';

import { DataService } from './services/data.service';
import { NumberOnlyDirective } from './directives/number-only.directive';
import { FindIndexPositionComponent } from './components/page-one/find-index-position/find-index-position.component';
import { FindIndexPositionPipe } from './pipes/find-index-position.pipe';
import { EllipsisPipe } from './pipes/ellipsis.pipe';
import { EllipsisComponent } from './components/page-one/ellipsis/ellipsis.component';


@NgModule({
  declarations: [
    AppComponent,
    ShellComponent,
    NotFoundComponent,
    PageOneComponent,
    PageTwoComponent,
    ReverseStringPipe,
    ReverseStringComponent,
    UpperCasePipe,
    UpperCaseComponent,
    NumberOnlyDirective,
    FindIndexPositionComponent,
    FindIndexPositionPipe,
    EllipsisPipe,
    EllipsisComponent
  ],
  imports: [
    HttpModule,
    CommonModule,
    FormsModule,
    BrowserModule,
    CustomMaterialModule,
    BrowserAnimationsModule,
    AppRoutingModule,
  ],
  providers: [
    DataService
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  bootstrap: [AppComponent]
})
export class AppModule { }
